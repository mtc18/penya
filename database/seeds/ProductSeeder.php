<?php

use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        DB::table('cathegories')->insert([
            'id'=>1,
            'name'=>'bebidas'
        ]);

        DB::table('products')->insert([
            'id'=>1,
            'name'=>'cerveza',
            'price'=>1.5,
            'cathegory_id'=>1,
        ]);
        DB::table('products')->insert([
            'id'=>2,
            'name'=>'Calimocho',
            'price'=>5,
            'cathegory_id'=>1,
        ]);

    }
}
