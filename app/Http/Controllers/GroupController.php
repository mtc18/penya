<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Session;//SESIOOOOOOOOOOOOOOOOOOOOOOOOOOOON

class GroupController extends Controller{

    public function index(Request $request){
        $group = $request->session()->get('group');

        if (! $group){
            $group = [];
        }

        return view('group.index', ['users' => $group]);
    }

    public function addUser(Request $request, $id){
        //buscar usuario
        $user = User::findOrFail($id);

        $group = $request->session()->get('group');

        if ($group == null) {
            $group = array();
        }

        $position = -1;
        foreach ($group as $key => $item) {
            if ($item->id == $id) {
                $item->cantidad++;
                $position = $key;
                break;
            }
        }

        if ($position == -1) {
            $user->cantidad = 1;
            $request->session()->push('group', $user);
        }
        return redirect('/groups');
    }

    public function flush(Request $request){//Vacia la lista
        $request->session()->forget('group');
        return back();
    }

    public function delete(Request $request,$id){//para borrar un elemento de Sesion
        $user=User::findOrFail($id);
        $group=$request->session()->get('group');

        foreach ($group as $key => $userSesion){
            /*userSesion son los usuarios que actualmente estan la group*/
            if ($user->id==$group[$key]->id){
                if ($userSesion->cantidad <=1){
                    $request->session()->forget('group.' .$key);
                }else{
                    $userSesion->cantidad--;
                }
                return back();
            }
        }
        return back();
    }//fin delete
}
