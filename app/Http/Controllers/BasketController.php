<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Order;
use App\User;
use \Carbon\Carbon;
use Session;//SESIONEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEES

class BasketController extends Controller{

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(Request $request){
        $basket = $request->session()->get('basket');

        if (! $basket){
            $basket = [];
        }

        return view('basket.index', ['products' => $basket]);
    }

    public function addProduct(Request $request, $id){
        $product = Product::findOrFail($id);

        $basket = $request->session()->get('basket');

        if ( $basket == null){
            $basket = array();
        }

        $position = -1;
        foreach ($basket as $key => $item){
            if ($item->id == $id) {
                $position = $key;
                break;
            }
        }

        if ($position == -1) {
            $product->cantidad =1;
            $request->session()->push('basket', $product);
        } else {
            $basket[$position]->cantidad++;
            $basket[$position]->importe=$product->price*$basket[$position]->cantidad;
        }

        return redirect('/basket');
    }//fin AddProduct

    public function flush(Request $request){//VACIA LA LISTA ENTERA
        $request->session()->forget('basket');
        return back();
    }//fin flush

    public function delete(Request $request,$id){//para borrar un elemento de Sesion
        $product=Product::findOrFail($id);
        $basket=$request->session()->get('basket');

        foreach ($basket as $key => $productSesion){
            /*productSesion son los productos que actualmente estan en la BASKET*/
            if ($product->id==$basket[$key]->id){
                if ($productSesion->cantidad <=1){
                    $request->session()->forget('basket.' .$key);
                }else{
                    $productSesion->cantidad--;
                }
                return back();
            }
        }
        return back();
    }//fin delete

    public function store(Request $request){
        $order = New Order;
        $basket = $request->session()->get('basket');
        $order->paid = 0;
        $order->date = Carbon::today();
        $order->user_id = Auth()->user()->id;

        $order->save();

        foreach ($basket as $products){
            $order->products()->attach($products->id, [
              'price' => $products->price,
              'quantity' => $products->cantidad,
          ]);
        }
        return redirect('/orders');
    }


}//controller
