<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Cathegory;

class CathegoryController extends Controller{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(){
      $this->middleware('auth')->except('index');
  }

  public function index(){
    $cathegories = Cathegory::paginate(10);
    return view('cathegory.index', ['cathegories' => $cathegories]);
}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        return view('cathegory.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $rules = [
            'name' => 'required|max:255|min:3',
        ];

        $request->validate($rules);

        $cathegory = new Cathegory();
        $cathegory->fill($request->all());
        $cathegory->save();

        return redirect('/cathegories');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){

        $products = Product::all();

        $cathegory=Cathegory::findOrFail($id);

        return view('cathegory.show', [
            'cathegory' => $cathegory
        ], ['products'=>$products]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        $cathegory = Cathegory::findOrFail($id);
        return view('cathegory.edit', ['cathegory' => $cathegory]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){

        $rules = [
            'name' => 'required|max:255|min:4',
        ];

        $request->validate($rules);


        $cathegory = Cathegory::findOrFail($id);
        $cathegory->fill($request->all());
        $cathegory->save();

        return redirect('/cathegories/' . $cathegory->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        Cathegory::destroy($id);
        return back();
    }
}
