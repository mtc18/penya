<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Cathegory;


class ProductController extends Controller{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(){
        $this->middleware('auth'); //aplicable a todos los métodos
    }

    public function index(){
        $products = Product::paginate(10);
        $cathegories=Cathegory::all();

        return view('product.index', ['products' => $products], ['cathegories'=>$cathegories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        $cathegories=Cathegory::all();
        return view('product.create',['cathegories' => $cathegories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        //validacion
        $rules = [
            'name' => 'required|max:255|min:3',
            'price' => 'required|numeric',
        ];

        $request->validate($rules);

        $product = new Product();
        $product->fill($request->all());
        $product->save();

        return redirect('/products');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product, Cathegory $cathegory){
        return view('product.show', [
        'product' => $product,
    ]);
 }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        $product = Product::findOrFail($id);
        $cathegories=Cathegory::all();
        return view('product.edit',[
            'cathegories' => $cathegories,
            'product' => $product
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        $rules = [
            'name' => 'required|max:255|min:4',
            'price' => 'required|numeric',
            'cathegory_id' => 'required',
        ];

        $request->validate($rules);

        $product = Product::findOrFail($id);
        $product->fill($request->all());
        $product->save();

        return redirect('/products/' . $product->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        Product::destroy($id);
        return back();
    }
}
