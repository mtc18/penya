<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\OrderProduct;
use App\Basket;
use App\Order;
use Session;

class OrderController extends Controller{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(Request $request){
        $orders = Order::paginate(10);

        return view('order.index', ['orders' => $orders]);

        if ($user->role_id==3 || $user->role_id==4){
            if ($request->all=='1') {
                return $this->listaTodos();
            }
        }else{
            return $this->listaPropios();
        }

        if ($user->role_id==2){
            return $this->listaPropios();
        }

    }

    public function listadePropios(){
        return 'propios';
    }

    public function listadeTodos(){
        return 'todos';
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){
        $order=Order::findOrFail($id);
        $products = $order->$orderProduct;
        $total=0;

        foreach ($products as $product){
            $total += $product->price*$product->pivot->quantity;
            $this->authorize('view', $order);
            return view('order.show', ['order'=>$order],['products'=>$products],['total'=>$total]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        //
    }
}
