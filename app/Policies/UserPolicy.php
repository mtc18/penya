<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\User  $model
     * @return mixed
     */
    public function view(User $user, User $model){

    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user){
        //
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\User  $model
     * @return mixed
     */
    public function update(User $user, User $model){
        //si ponemos return true; SE PODRÁ MODIFICAR
        return $user->id==$model->id;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\User  $model
     * @return mixed
     */
    public function delete(User $user, User $model){
        //solo administrador y root puede borrar y NO a sí mismo
        return ($user->id != $model->id /*esto es que user NO se pueda borrar a si mismo */)
        && ($user->role_id==3 || $user->role_id==4 /*esto es para que solo pueden acceder a esto los roles 3 y 4 (administrador y root) */);
    }

    public function index(User $user){
        return ($user->role_id!=1);/*esto es para que solo pueden acceder a esto los roles 2,3 y 4 (socio, administrador y root) */
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\User  $user
     * @param  \App\User  $model
     * @return mixed
     */
    public function restore(User $user, User $model){
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\User  $model
     * @return mixed
     */
    public function forceDelete(User $user, User $model){
        //
    }
}
