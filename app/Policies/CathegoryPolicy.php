<?php

namespace App\Policies;

use App\User;
use App\Cathegory;
use Illuminate\Auth\Access\HandlesAuthorization;

class CathegoryPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the cathegory.
     *
     * @param  \App\User  $user
     * @param  \App\Cathegory  $cathegory
     * @return mixed
     */
    public function view(User $user, Cathegory $cathegory)
    {
        //
    }

    /**
     * Determine whether the user can create cathegories.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the cathegory.
     *
     * @param  \App\User  $user
     * @param  \App\Cathegory  $cathegory
     * @return mixed
     */
    public function update(User $user, Cathegory $cathegory)
    {
        //
    }

    /**
     * Determine whether the user can delete the cathegory.
     *
     * @param  \App\User  $user
     * @param  \App\Cathegory  $cathegory
     * @return mixed
     */
    public function delete(User $user, Cathegory $cathegory)
    {
        //
    }

    /**
     * Determine whether the user can restore the cathegory.
     *
     * @param  \App\User  $user
     * @param  \App\Cathegory  $cathegory
     * @return mixed
     */
    public function restore(User $user, Cathegory $cathegory)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the cathegory.
     *
     * @param  \App\User  $user
     * @param  \App\Cathegory  $cathegory
     * @return mixed
     */
    public function forceDelete(User $user, Cathegory $cathegory)
    {
        //
    }
}
