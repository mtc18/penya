<?php

namespace App\Policies;

use App\User;
use App\role;
use Illuminate\Auth\Access\HandlesAuthorization;

class RolePolicy{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the role.
     *
     * @param  \App\User  $user
     * @param  \App\role  $role
     * @return mixed
     */


    public function index(User $user){
       return true;
    }

    public function view(User $user, role $role){
        //permite ver un objeto
        /*        return true; //para ver todo/*/
        return $user->role_id==$role->id;
    }

    /**
     * Determine whether the user can create roles.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user){
        //
    }

    /**
     * Determine whether the user can update the role.
     *
     * @param  \App\User  $user
     * @param  \App\role  $role
     * @return mixed
     */
    public function update(User $user, role $role)
    {
        //
    }

    /**
     * Determine whether the user can delete the role.
     *
     * @param  \App\User  $user
     * @param  \App\role  $role
     * @return mixed
     */
    public function delete(User $user, role $role)
    {
        //
    }

    /**
     * Determine whether the user can restore the role.
     *
     * @param  \App\User  $user
     * @param  \App\role  $role
     * @return mixed
     */
    public function restore(User $user, role $role)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the role.
     *
     * @param  \App\User  $user
     * @param  \App\role  $role
     * @return mixed
     */
    public function forceDelete(User $user, role $role)
    {
        //
    }
}
