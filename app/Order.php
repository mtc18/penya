<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model{

    protected $fillable = [
        'id', 'paid', 'date','user_id'
    ];

    // public function orderProduct(){
    //     return $this->belongsTo(\App\OrderProduct::class);
    // }

    public function products(){
        return $this->belongsToMany(\App\Product::class);
    }

    public function user(){
        return $this->belongsTo(\App\User::class);
    }

    public function total(){
        $total=0;

        foreach ($this->products as $product){
            $total += $product->price * $product->quantity;
        }
        return $total;
    }
}
