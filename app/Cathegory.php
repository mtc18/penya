<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cathegory extends Model{

    protected $fillable = [
        'id', 'name'
    ];

    public function products (){
        return $this ->hasMany(Product::class);
   }

}
