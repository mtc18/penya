<?php
Use Carbon\Carbon;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('users', 'UserController@index')->name('usuarios');
// Route::get('users/{id}', 'UserController@show');
// Route::get('users/{id}/edit', 'UserController@edit');
// Route::get('users/create', 'UserController@create');
// Route::put('users/{id}', 'UserController@update');
// Route::delete('users/{id}', 'UserController@destroy');
// Route::post('users', 'UserController@store');
// Route::get('users/create', 'UserController@create');

//Ruta de tipo resource: equivale a las 7 rutas REST
//Ruta especial antes que resource, si no "show...."
Route::get('users/especial', 'UserController@especial');
Route::get('users/{id}/edit2', 'UserController@edit2');
Route::resource('users', 'UserController');

Route::resource('products', 'ProductController');
Route::resource('cathegories', 'CathegoryController');
Route::resource('roles', 'RoleController');

Route::get('/groups', 'GroupController@index');
Route::get('/groups/flush', 'GroupController@flush');
Route::delete('/groups/{id}', 'GroupController@delete');
Route::get('/groups/{id}', 'GroupController@addUser');

Route::get('/basket', 'BasketController@index');
Route::get('/basket/flush', 'BasketController@flush');
Route::get('/basket/delete/{id}', 'BasketController@delete');
Route::post('basket', 'BasketController@store');
Route::get('/basket/{id}', 'BasketController@addProduct');

Route::get('/orders', 'OrderController@index');
Route::get('/orders', 'OrderController@index');
Route::get('/orders', 'OrderController@index');
Route::resource('/orders', 'OrderController');


Route::get('/hoy', function(){
    $today=Carbon::today();
    return $today->format('d-m-Y');
});


////////////////////////////////////////////
//Ejemplos de rutas con funciones anónimas:
////////////////////////////////////////////

Route::get('usuarios/{id}', function ($id) {
 return "Detalle del usuario $id";
});


Route::get('usuarios/{id}', function ($id) {
 return "Detalle del usuario $id";
})->where('id', '[0-9]+');

Route::get('usuarios/{id}/{name?}', function ($id, $name=null) {
    if($name) {
        return "Detalle del usuario $id. El nombre es $name";
    } else {
        return "Detalle del usuario $id. Anónimo";
    }
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
