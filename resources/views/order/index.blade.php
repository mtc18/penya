@extends('layouts.app')
@section('content')
<div class="container">
  <div class="row content">
    <div class="col-sm-12 text-left">
      <h1>Lista de Pedidos</h1>
      <table  class="table table-striped table-hover">
        <thead>
          <tr>
            <th>Id</th>
            <th>Fecha</th>
            <th>User</th>
            <th>Acciones</th>
          </tr>
        </thead>
        <tbody>
          @forelse ($orders as $order)
          <tr>
            <td>{{ $order->id }}</td>
            <td>{{ $order->date }}</td>
            <td>{{ $order->user->name }}</td>
            <td>
              <form method="post" action="/orders/{{ $order->id }}">
                <a class="btn btn-primary"  role="button"
                href="/orders/{{ $order->id }}">
                Ver
              </a>
            </td>
          </tr>
          @empty
          <tr><td colspan="4">No hay pedidos</td></tr>
          @endforelse
        </tbody>
      </table>
    </div>
  </div>
  {{ $orders->render() }}
</div>
@endsection
