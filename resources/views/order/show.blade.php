@extends('layouts.app')

@section('title', 'Order')

@section('content')

<h1>Este es el detalle del Pedido <?php echo $order->id ?></h1>

<table  class="table table-striped table-hover">
    <thead>
      <tr>
        <th>Id</th>
        <th>Nombre</th>
        <th>Pagado</th>
    </tr>
</thead>
<tbody>
    <td>{{ $order->id }}</td>
    <td>{{ $order->name }}</td>
    <td>{{ $order->paid }}</td>
</tbody>
</table>


