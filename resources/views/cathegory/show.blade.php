@extends('layouts.app')

@section('title', 'Cathegories')

@section('content')

<h1>Este es el detalle de la Categoria <?php echo $cathegory->name ?></h1>

<table  class="table table-striped table-hover">
    <thead>
      <tr>
        <th>Id</th>
        <th>Nombre</th>
    </tr>
</thead>
<tbody>
    <td>{{ $cathegory->id }}</td>
    <td>{{ $cathegory->name }}</td>
</tbody>
</table>

<hr>
<h2>Productos de la Categoria: </h2>

@foreach($cathegory->products as $product)
<li>{{$product->name}}-{{$product->price}}</li>
@endforeach

@endsection

