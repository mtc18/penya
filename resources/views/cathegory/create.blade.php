@extends('layouts.app')

@section('title', 'Cathegories')

@section('content')

<h1>Nueva Categoria</h1>

<form method="post" action="/cathegories">
    {{ csrf_field() }}

    <label>Nombre</label>
    <input type="text" name="name" value="{{ old('name') }}">
    <div class="alert alert-danger">
        {{$errors->first('name')}}
    </div>


<input type="submit" value="Nuevo">
</form>
@endsection
