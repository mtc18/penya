@extends('layouts.app')

@section('title', 'Cathegories')

@section('content')
<h1>Editar Product</h1>

<form method="post" action="/cathegories/{{ $cathegory->id }}">
    {{ csrf_field() }}

    <input type="hidden" name="_method" value="PUT">

    <label>Name</label>
    <input type="text" name="name"
    value="{{ old('name') ? old('name') : $cathegory->name }}">
    <div class="alert alert-danger">
        {{ $errors->first('name') }}
    </div>
    <br>
    <br>

    <input type="submit" value="Guardar Cambios">
</form>
@endsection
