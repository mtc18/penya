@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row content">
    <div class="col-sm-12 text-left">
      <h1>Lista de Categorias</h1>
      <div class="alert">
        <a href="/cathegories/create" class="btn btn-primary">Nuevo</a>
      </div>

      <table  class="table table-striped table-hover">
        <thead>
          <tr>
            <th>Nombre</th>
            <th>Id</th>
            <th>Opciones</th>
          </tr>
        </thead>
        <tbody>
          @forelse ($cathegories as $cathegory)
          <tr>
            <td>{{ $cathegory->name }}</td>
            <td>{{ $cathegory->id }}</td>
            <td>

              <form method="post" action="/cathegories/{{ $cathegory->id }}">
                <a class="btn btn-primary"  role="button"
                href="/cathegories/{{ $cathegory->id }}/edit">
                Editar
              </a>
              <a class="btn btn-primary"  role="button"
                href="/cathegories/{{ $cathegory->id }}">
                Ver
              </a>
              {{ csrf_field() }}
              <input type="hidden" name="_method" value="DELETE">
              <input type="submit" value="Borrar" class="btn btn-primary">
            </form>
          </td>
        </tr>
        @empty
        <tr><td colspan="4">No hay categorias</td></tr>
        @endforelse
      </tbody>
    </table>

    {{ $cathegories->render() }}
  </div>
</div>
</div>
@endsection
