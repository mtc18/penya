@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row content">
    <div class="col-sm-12 text-left">
      <h1>Lista de Productos</h1>
      <div class="alert">
        <a href="/products/create" class="btn btn-primary">Nuevo</a>
      </div>

      <table  class="table table-striped table-hover">
        <thead>
          <tr>
            <th>Nombre</th>
            <th>Precio</th>
            <th>Categoria</th>
            <th>Opciones</th>
          </tr>
        </thead>
        <tbody>
          @forelse ($products as $product)
          <tr>
            <td>{{ $product->name }}</td>
            <td>{{ $product->price }}</td>
            <td>
              @foreach($cathegories as $cathegory)
              @if($product->cathegory_id==$cathegory->id)
              {{$cathegory->name}}
              @endif
              @endforeach
            </td>
            <td>
              <form method="post" action="/products/{{ $product->id }}">
                <a class="btn btn-primary"  role="button"
                href="/products/{{ $product->id }}/edit">
                Editar
              </a>
              <a class="btn btn-primary"  role="button"
              href="/products/{{ $product->id }}">
              Ver
            </a>
            <form method="post" action="/basket/{{ $product->id }}">
              <a class="btn btn-success"  role="button"
              href="/basket/{{ $product->id }}">
              Añadir a Cesta
            </a>
          </form>
          {{ csrf_field() }}
          <input type="hidden" name="_method" value="DELETE">
          <input type="submit" value="Borrar" class="btn btn-primary">
        </form>
      </td>
    </tr>
    @empty
    <tr><td colspan="4">No hay productos</td></tr>
    @endforelse
  </tbody>
</table>

{{ $products->render() }}
</div>
</div>
</div>
@endsection
