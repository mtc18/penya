@extends('layouts.app')

@section('title', 'Products')

@section('content')
<style type="text/css">
.alert {
  padding: 5px;
  background-color: #faa; /* Red */
  margin: 5px;
}
</style>
<h1>Nuevo Producto</h1>


<form method="post" action="/products">
    {{ csrf_field() }}

    <label>Nombre</label>
    <input type="text" name="name" value="{{ old('name') }}">
    <div class="alert alert-danger">
        {{ $errors->first('name') }}
    </div>
    <br>

    <label>Price</label>
    <input type="text" name="price" value="{{ old('price') }}">
    <div class="alert alert-danger">
        {{ $errors->first('price') }}
    </div>

    <br>

    <label> Cathegory: </label>
    <select name="cathegory_id">
        @foreach ($cathegories as $cathegory)
        <option value="{{ $cathegory->id }}"
            {{ old('cathegory_id') == $cathegory?
            'selected="selected"' :
            ''
        }}>{{ $cathegory->name }}
    </option>
    @endforeach
    <div class="alert alert-danger">
        {{ $errors->first('cathegory_id') }}
    </div>
</select>
<br>
<br>
<input type="submit" value="Nuevo">
</form>
@endsection
