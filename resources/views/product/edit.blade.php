@extends('layouts.app')

@section('title', 'Products')

@section('content')
<h1>Editar Product</h1>

<form method="post" action="/products/{{ $product->id }}">
    {{ csrf_field() }}

    <input type="hidden" name="_method" value="PUT">

    <label>Name</label>
    <input type="text" name="name"
    value="{{ old('name') ? old('name') : $product->name }}">
    <div class="alert alert-danger">
        {{ $errors->first('name') }}
    </div>
    <br>
    <br>

    <label>Price</label>
    <input type="text" name="price"
    value="{{ old('price') ? old('price') : $product->price }}">
    <div class="alert alert-danger">
        {{ $errors->first('price') }}
    </div>
    <br>
    <br>

    <label> Cathegory: </label>
    <select name="cathegory_id">
        @foreach ($cathegories as $cathegory)
        <option value="{{ $cathegory->id }}"
            {{ old('cathegory_id') == $cathegory?
            'selected="selected"' :
            ''
        }}>{{ $cathegory->name }}
    </option>
    @endforeach
    <div class="alert alert-danger">
        {{ $errors->first('cathegory_id') }}
    </div>
</select> <br>

<input type="submit" value="Guardar Cambios">
</form>
@endsection
