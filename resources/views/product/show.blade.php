@extends('layouts.app')

@section('title', 'Products')

@section('content')

<h1>
    Este es el detalle del Producto <?php echo $product->name ?>
</h1>

<table  class="table table-striped table-hover">
    <thead>
      <tr>
        <th>Id</th>
        <th>Nombre</th>
        <th>Precio</th>
        <th>ID Categoria</th>
    </tr>
</thead>
<tbody>
    <td>{{ $product->id }}</td>
    <td>{{ $product->name }}</td>
    <td>{{ $product->price }}</td>
    <td>{{ $product->cathegory_id }}</td>
</tbody>
</table>

@endsection

