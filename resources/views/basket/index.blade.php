@extends('layouts.app')
@section('content')
<div class="container">
  <div class="row content">
    <div class="col-sm-12 text-left">
      <h1>Basket</h1>
      <table  class="table table-striped table-hover">
        <thead>
          <tr>
            <th>Nombre</th>
            <th>Id</th>
            <th>Cantidad</th>
            <th>Precio</th>
            <th>Importe</th>
            <th>Opciones</th>
          </tr>
        </thead>
        <tbody>
          @forelse ($products as $product)
          <tr>
            <td>{{ $product->name }}</td>
            <td>{{ $product->id }}</td>
            <td><a class="btn btn-success" href="/basket/{{ $product->id }}">+</a>{{$product->cantidad}}<a class="btn btn-success" href="/basket/delete/{{ $product->id }}">-</a></td>
            <td>{{ $product->price }}</td>
            <td>{{ $product->importe }}</td>
            <td>
             <a href="/basket/delete/{{ $product->id}}" class="btn btn-primary">Borrar</a>
             <form method="post" action="/products/{{ $product->id }}">
              <a class="btn btn-primary"  role="button"
              href="/products/{{ $product->id }}">
              Ver
            </a>
          </form>
        </td>
      </tr>
      @empty
      <tr><td colspan="4">No hay Productos en la cesta</td></tr>
      @endforelse
    </tbody>
  </table>
  <a href="/basket/flush" class="btn btn-danger">Vaciar Lista</a>
  <form method="post" action="/basket">
    {{ csrf_field() }}
    <input type="submit" name="" value="Finalizar Pedido" class=" btn btn-success">
  </form>

</div>
</div>
</div>
@endsection
