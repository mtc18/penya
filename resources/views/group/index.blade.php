@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row content">
    <div class="col-sm-12 text-left">
      <h1>Groups (Usuarios en Sesion)</h1>
      <table  class="table table-striped table-hover">
        <thead>
          <tr>
            <th>Nombre</th>
            <th>Email</th>
            <th>Role</th>
            <th>Cantidad</th>
            <th>Opciones</th>
          </tr>
        </thead>
        <tbody>
          @forelse ($users as $user)
          <tr>
            <td>{{ $user->name }}</td>
            <td>{{ $user->email }}</td>
            <td>{{ $user->role->name }}</td>
            <td>{{ $user->cantidad }}</td>
            <td>
             <form method="post" action="/groups/{{ $user->id }}">
               {{ csrf_field() }}
               @can('delete', $user)
               <input type="hidden" name="_method" value="DELETE">
               <input type="submit" value="Borrar" class="btn btn-primary">
               @endcan
             </form>
             <form method="post" action="/users/{{ $user->id }}">
              <a class="btn btn-primary"  role="button"
              href="/users/{{ $user->id }}">
              Ver
            </a>
          </form>
        </td>
      </tr>
      @empty
      <tr><td colspan="4">No hay usuarios en Sesion</td></tr>
      @endforelse
    </tbody>
  </table>
  <a href="/groups/flush" class="btn btn-primary">Vaciar Lista</a>
</div>
</div>
</div>
@endsection
