@extends('layouts.app')

@section('title', 'Usuarios')

@section('content')

<h1>
    Este es el detalle del usuario <?php echo $user->name ?>
</h1>
<table  class="table table-striped table-hover">
    <thead>
      <tr>
        <th>ID</th>
        <th>Nombre</th>
        <th>Email</th>
        <th>Role</th>
    </tr>
</thead>
<tbody>
    <td>{{ $user->id }}</td>
    <td>{{ $user->name }}</td>
    <td>{{ $user->email }}</td>
    <td>{{ $user->role->name }}</td>
</tbody>
</table>

@endsection

