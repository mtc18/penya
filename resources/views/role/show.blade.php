@extends('layouts.app')

@section('title', 'Roles')

@section('content')

<h1>
    Este es el detalle del Role <?php echo $role->id ?>
</h1>

<ul>
    <li>Nombre: {{ $role->name }}</li>
</ul>

<h2>Lista de Usuarios</h2>
<ul>
    @foreach($role->users as $user)
    <li>{{ $user->name }}/{{ $user->email }}
    </li>
    @endforeach
</ul>
@endsection

