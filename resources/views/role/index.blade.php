@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row content">
    <div class="col-sm-12 text-left">
      <h1>Lista de Roles</h1>
    </div>

    <table  class="table table-striped table-hover">
      <thead>
        <tr>
          <th>Nombre</th>
          <th>Id</th>
          <th>Opciones</th>
        </tr>
      </thead>
      <tbody>
        @forelse ($roles as $role)
        <tr>
          <td>{{ $role->name }}</td>
          <td>{{ $role->id }}</td>
          <td>
            <form method="post" action="/roles/{{ $role->id }}">
              @can('view', $role) {{-- asi solo nos deja ver NUESTRO role --}}
              <a class="btn btn-primary"  role="button"
              href="/roles/{{ $role->id }}">
              Ver
            </a>
            @endcan
          </form>
        </td>
      </tr>
      @empty
      <tr><td colspan="4">No hay roles</td></tr>
      @endforelse
    </tbody>
  </table>
  {{ $roles->render() }}
</div>
</div>
</div>
@endsection
